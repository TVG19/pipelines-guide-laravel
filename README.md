# Pipelines with Laravel

Pipelines allows you to put your Bitbucket hosted code to work. It enables you to build, test, and deploy your code using the cloud and the principals of [CI/CD](https://www.atlassian.com/continuous-delivery/ci-vs-ci-vs-cd). You might like to run tests triggered by any git push to Bitbucket, to confirm that your commit did not introduce any new problems. Or, you could deploy a new version of your code, automatically, whenever your tests complete successfully; turning on features at your leisure using feature flags. Let's get started! 

This is an example repo showing [Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/build-test-and-deploy-with-pipelines-792496469.html) usage with Laravel. 

Other Pipeline example repos:

  * [Java](https://bitbucket.org/bootcamp-pipelines/pipelines-java/src/master/)

  * [Laravel](https://bitbucket.org/bootcamp-pipelines/pipelines-laravel/src/master/)

  * [Node](https://bitbucket.org/bootcamp-pipelines/pipelines-node/src/master/)

  * [PHP](https://bitbucket.org/bootcamp-pipelines/pipelines-php/src/master/)

  * [Ruby](https://bitbucket.org/bootcamp-pipelines/pipelines-ruby/src/master/)

[Pipeline's Laravel reference page](https://confluence.atlassian.com/bitbucket/laravel-with-bitbucket-pipelines-913473967.html)


## Setup
For this demo, all you need to do is import this repo and enable pipelines to get your first pipeline to run!


1. Import this repository by: 

     * clicking the plus sign

     * under import select **Repository**

     * paste this url: [https://bitbucket.org/bitbucketpipelines/pipelines-guide-laravel/src/master/](https://bitbucket.org/bitbucketpipelines/pipelines-guide-laravel/src/master/)

     * give your imported repo a name

    ![Import gif](images/import.gif)

2. On the left navigation bar, click **Pipelines** and scroll down to view the bitbucket-pipelines.yml file. Click **Enable**

    ![Enable gif](images/enable.gif)

3. Watch your build run!

    ![Build gif](images/build.gif)

**Note:** Bitbucket Pipelines includes fifty free minutes per account, at the time of writing. You can check your team or account's minutes usage for the month by clicking your **Avatar** > **Bitbucket settings** > **Plan details**.


## Basic Commands

File: `bitbucket-pipelines.yml`


```
image: php:7-fpm

pipelines:
  default:order
    - step:
        script:
          - apt-get update && apt-get install -qy git curl libmcrypt-dev mysql-client
          - yes | pecl install mcrypt-1.0.1
          - docker-php-ext-install pdo_mysql
          - bash ./install-composer.sh
          - composer install
          - ln -f -s .env.pipelines .env
          - php artisan migrate
          - php artisan serve &
          - sleep 5
          - ./vendor/bin/phpunit
          - curl -vk http://localhost:8000
```

For in-depth configuration information, visit the Bitbucket Pipelines [YAML Configuration Page](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html). A list of the more common keywords are listed below.

`step`: Each step loads a new Docker container that includes a clone of the current repository.

`script`: A list of commands that are run, in order.

`caches`: Store downloaded dependencies on our server, to avoid re-downloading for each step.

`artifacts`: Defines files that are produced by a step, such as reports and JAR files, that you want to share with a following step.

## Advanced Commands

Connecting to a database can be as simple as adding a few lines to `bitbucket-pipelines.yml` to specify and define the database.

The resulting file may looks like this:

```
image: php:7-fpm

pipelines:
  default:order
    - step:
        script:
          - apt-get update && apt-get install -qy git curl libmcrypt-dev mysql-client
          - yes | pecl install mcrypt-1.0.1
          - docker-php-ext-install pdo_mysql
          - bash ./install-composer.sh
          - composer install
          - ln -f -s .env.pipelines .env
          - php artisan migrate
          - php artisan serve &
          - sleep 5
          - ./vendor/bin/phpunit
          - curl -vk http://localhost:8000
        services:
          - mysql
definitions:
  services:
    mysql:
      image: mysql:5.7
      environment:
        MYSQL_DATABASE: 'homestead'
        MYSQL_RANDOM_ROOT_PASSWORD: 'yes'
        MYSQL_USER: 'homestead'
        MYSQL_PASSWORD: 'secret'
```
The mysql enviroment variables should be the same as defined in the .env.pipelines file.

## Running the repo locally

    cd quickstart

    composer install

    php artisan migrate

    php artisan serve

[Complete Tutorial](https://laravel.com/docs/5.2/quickstart)